const { error } = require('console');
const fs = require('fs');
// fetchData('./lipsum.txt')

//1. Read the given file lipsum.txt

function readGivenFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'UTF-8', (error, fileData) => {
            if (error) {
                reject(error);
            } else {
                resolve(fileData);
            }
        });
    });
}

function writeToGivenLoc(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, JSON.stringify(data), (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(fileName)
            }
        });
    });
}

function appendToGivenFile(fileName, appendFileName) {
    return new Promise((resolve, reject) => {

        fs.appendFile(appendFileName, fileName + ' ', (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(fileName);
            }
        });
    });
}

function deleteFile(fileArrayName) {

    return new Promise((resolve, reject) => {

        for (let index = 0; index < fileArrayName.length - 1; index++) {
            //console.log(fileArrayName[i]);
            fs.unlink(fileArrayName[index], (error) => {
                if (error) {
                    reject(error);
                } else {
                    console.log('main file deleted')
                }
            });
        }
    });
}

function fetchData(fileName) {


    let upperCaseFile = './upperCaseFile.txt';
    let appendedFile = './filenames.txt';
    let lowerCaseFile = './lowerCaseFile.txt';
    let sortedDataFile = './sortedDataFile.txt';

    readGivenFile(fileName)
        .then((fileData) => {
            return writeToGivenLoc(upperCaseFile, fileData.toUpperCase());
        })

        .then((upperCaseFile) => {
            return appendToGivenFile(upperCaseFile, appendedFile);
        })
        .then((upperCaseFile) => {
            return readGivenFile(upperCaseFile);
        })
        .then((upperCaseData) => {
            return writeToGivenLoc(lowerCaseFile, upperCaseData.toLowerCase().split('. '));
        })
        .then((lowerCaseFile) => {
            return appendToGivenFile(lowerCaseFile, appendedFile);
        })
        .then((lowerCaseFile) => {
            return readGivenFile(lowerCaseFile);
        })
        .then((lowerCaseData) => {
            return writeToGivenLoc(sortedDataFile, JSON.parse(lowerCaseData).sort());
        })
        .then((sortedDataFile) => {
            return appendToGivenFile(sortedDataFile, appendedFile);
        })
        .then(() => {
            return readGivenFile(appendedFile);
        })
        .then((appendedFileData) => {
            deleteFile(appendedFileData.split(' '));
        })
        .catch((error)=>{
            console.log(error);
        });


    // 2. Convert the content to uppercase & write to a new file.
    //    Store the name of the new file in filenames.txt
    // 3. Read the new file and convert it to lower case.
    //    Then split the contents into sentences. Then write it to a new file.
    // 4. Read the new files, sort the content, write it out to a new file.
    //    Store the name of the new file in filenames.txt
    // 5. Read the contents of filenames.txt and delete all
}
module.exports = fetchData;