const fs = require('fs');
const path = require('path');


function makeDirectory(dirPath, noOfFile) {
    return new Promise((resolve, reject) => {
        fs.mkdir(dirPath, (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(noOfFile);
            }
        });
    });
}

function creteFile(dirPath, noOfFile) {
    return new Promise((resolve, reject) => {
        let count = 0;
        const filePathArray = [];
        for (let fileNumber = 1; fileNumber <= noOfFile; fileNumber++) {
            let filePath = path.join(dirPath, `${fileNumber}.json`);
            let data = `${fileNumber}.json file is created`;
            fs.writeFile(filePath, data, (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    count++;
                    filePathArray.push(filePath);
                    console.log("new file is created");
                }
                if (count == noOfFile) {
                    console.log("now time to delete ");
                    resolve(filePathArray);
                }
            });
        }
    });
}

function deleteFiles(dirPath, filePathArray) {
    return new Promise((resolve, reject) => {
    for (let fileNumber = 1; fileNumber <= filePathArray.length; fileNumber++) {
        let filePath = path.join(dirPath, `${fileNumber}.json`);
        fs.unlink(filePath, (error) => {
            if (error) {
                reject("error ocuured to remove file" + error);
            }
            else{
                console.log("All files removed");
            }
        });
    }
});
}

function createDirectoryFiles(dirPath, noOfFile) {
    makeDirectory(dirPath, noOfFile)
        .then((noOfFile) => {
            return creteFile(dirPath, noOfFile);
        })
        .then((filePathArray) => {
            return deleteFiles(dirPath, filePathArray);
        })
        .catch((err) => {
            console.error(err);
        });
}
module.exports = createDirectoryFiles;